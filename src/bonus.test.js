import {calculateBonuses} from "./bonus-system";


describe('Standard program bonuses tests', () => {
    let app;
    console.log("Tests started");


    test('Standard program tests < 10000', (done) => {
        for (let i = -8000; i < 10000; i += 4000) {
            expect(calculateBonuses("Standard", i)).toEqual(0.05);
        }
        done()
    });

    test('Standard program tests < 50000', (done) => {
        for (let i = 10000; i < 50000; i += 10000) {
            expect(calculateBonuses("Standard", i)).toEqual(1.5 * 0.05);
        }
        done()
    });

    test('Standard program tests < 100000', (done) => {
        for (let i = 50000; i < 100000; i += 10000) {
            expect(calculateBonuses("Standard", i)).toEqual(2 * 0.05);
        }
        done()
    });

    test('Standard program tests > 100000', (done) => {
        expect(calculateBonuses("Standard", 100000)).toEqual(2.5 * 0.05);
        expect(calculateBonuses("Standard", 1337000)).toEqual(2.5 * 0.05);
        expect(calculateBonuses("Standard", Number.MAX_SAFE_INTEGER)).toEqual(2.5 * 0.05);
        done()
    });

    console.log('Tests Finished');

});


describe('Premium program bonuses tests', () => {
    let app;
    console.log("Tests started");


    test('Premium program tests < 10000', (done) => {
        for (let i = -8000; i < 10000; i += 4000) {
            expect(calculateBonuses("Premium", i)).toEqual(0.1);
        }
        done()
    });

    test('Premium program tests < 50000', (done) => {
        for (let i = 10000; i < 50000; i += 10000) {
            expect(calculateBonuses("Premium", i)).toEqual(1.5 * 0.1);
        }
        done()
    });

    test('Premium program tests < 100000', (done) => {
        for (let i = 50000; i < 100000; i += 10000) {
            expect(calculateBonuses("Premium", i)).toEqual(2 * 0.1);
        }
        done()
    });

    test('Premium program tests > 100000', (done) => {
        expect(calculateBonuses("Premium", 100000)).toEqual(2.5 * 0.1);
        expect(calculateBonuses("Premium", 1337000)).toEqual(2.5 * 0.1);
        expect(calculateBonuses("Premium", Number.MAX_SAFE_INTEGER)).toEqual(2.5 * 0.1);
        done()
    });

    console.log('Tests Finished');

});

describe('Diamond program bonuses tests', () => {
    let app;
    console.log("Tests started");


    test('Diamond program tests < 10000', (done) => {
        for (let i = -8000; i < 10000; i += 4000) {
            expect(calculateBonuses("Diamond", i)).toEqual(0.2);
        }
        done()
    });

    test('Diamond program tests < 50000', (done) => {
        for (let i = 10000; i < 50000; i += 10000) {
            expect(calculateBonuses("Diamond", i)).toEqual(1.5 * 0.2);
        }
        done()
    });

    test('Diamond program tests < 100000', (done) => {
        for (let i = 50000; i < 100000; i += 10000) {
            expect(calculateBonuses("Diamond", i)).toEqual(2 * 0.2);
        }
        done()
    });

    test('Diamond program tests > 100000', (done) => {
        expect(calculateBonuses("Diamond", 100000)).toEqual(2.5 * 0.2);
        expect(calculateBonuses("Diamond", 1337000)).toEqual(2.5 * 0.2);
        expect(calculateBonuses("Diamond", Number.MAX_SAFE_INTEGER)).toEqual(2.5 * 0.2);
        done()
    });

    console.log('Tests Finished');

});


describe('Nonsense program bonuses tests', () => {
    let app;
    console.log("Tests started");


    test('Nonsense program tests < 10000', (done) => {
        for (let i = -8000; i < 10000; i += 4000) {
            expect(calculateBonuses("Nonsense", i)).toEqual(0);
        }
        done()
    });

    test('Nonsense program tests < 50000', (done) => {
        for (let i = 10000; i < 50000; i += 10000) {
            expect(calculateBonuses("Nonsense", i)).toEqual(0);
        }
        done()
    });

    test('Nonsense program tests < 100000', (done) => {
        for (let i = 50000; i < 100000; i += 10000) {
            expect(calculateBonuses("Nonsense", i)).toEqual(0);
        }
        done()
    });

    test('Nonsense program tests > 100000', (done) => {
        expect(calculateBonuses("Nonsense", 100000)).toEqual(0);
        expect(calculateBonuses("Nonsense", 1337000)).toEqual(0);
        expect(calculateBonuses("Nonsense", Number.MAX_SAFE_INTEGER)).toEqual(0);
        done()
    });

    console.log('Tests Finished');

});